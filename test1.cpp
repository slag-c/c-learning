#include<iostream>
using namespace std;


//template<class T1,class T2,class T3>
//void test(T1 val1, T2 val2)
//{
//	cout << val1 + val2 << endl;
//}

//void test()
//{
//	cout << endl;
//}
//void test(string s1)
//{
//	cout << s1 << endl;
//	test();
//}
//
//void test(char ch, string s1)
//{
//	cout << ch << endl;
//	test(s1);
//}
//
//void test(int t, char ch,string s1)
//{
//	cout << t << endl;
//	test(ch,s1);
//}
//
////声明Args这是一个模版参数包 可以传过来0到任意个模版参数
//template<class ...Args>
////arg就是函数形参参数包
//void test(Args ...arg)
//{
//	test(arg...);
//}
//
//int main()
//{
//	test(1, 'a',"ggg");
//	return 0;
//}



//int main()
//{
//	int a = 6, b = 4;
//	// 拷贝x到捕捉列表中，利用mutable取消拷贝后x的常性，可以改变x的拷贝值
//	int x = 10;
//	auto add_x = [x](int a)mutable{x *= 2;return a + x;};
//	cout << add_x(10) << endl;
//	return 0;
//}

//class Rate
//{
//public:
//	Rate(double rate) : _rate(rate)
//	{}
//	double operator()(double money, int year)
//	{
//		return money * _rate * year;
//	}
//private:
//	double _rate;
//};
//int main()
//{
//	// 仿函数对象
//	double rate = 0.49;
//	Rate r1(rate);
//	r1(10000, 2);
//	// lamber表达式
//	auto r2 = [=](double monty, int year)->double {return monty * rate * year;};
//	cout << typeid(r2).name() << endl;
//	r2(10000, 2);
//	return 0;
//}


//int main()
//{
//	int x = 10;
//	int y = 20;
//	auto add1 = [=] {return x + y; };
//	auto add2 = [=] {return x + y; };
//	cout << typeid(add1).name() << endl;
//	cout << typeid(add2).name() << endl;
//	return 0;
//}


//#include<functional>
//template<class F, class T>
//T useF(F f, T x)
//{
//	static int count = 0;
//	cout << "count:" << ++count << endl;
//	cout << "count:" << &count << endl;
//	return f(x);
//}
//double f(double i)
//{
//	return i / 2;
//}
//struct Functor
//{
//	double operator()(double d)
//	{
//		return d / 3;
//	}
//};
//int main()
//{
//	// 函数名
//	std::function<double(double)> f1 = f;
//	cout << useF(f1, 11.11) << endl;
//	// 函数对象
//	std::function<double(double)> f2 = Functor();
//	cout << useF(f2, 11.11) << endl;
//	// lamber表达式
//	std::function<double(double)> f3 = [](double d)->double { return d / 4; };
//	cout << useF(f3, 11.11) << endl;
//	return 0;
//}


//// 使用举例
//#include <functional>
//int Plus(int a, int b)
//{
//	return a + b;
//}
//class Sub
//{
//public:
//	int sub(int a, int b)
//	{
//		return a - b;
//	}
//};
//int main()
//{
//	//表示绑定函数plus 参数分别由调用 func1 的第一，二个参数指定
//	std::function<int(int, int)> func1 = std::bind(Plus, placeholders::_1,
//		placeholders::_2);
//	//auto func1 = std::bind(Plus, placeholders::_1, placeholders::_2);
//	//表示绑定函数 plus 的第一，二为： 1， 2
//	cout << func1(1, 2) << endl;
//	
//	Sub s;
//	// 绑定成员函数,需要取类中成员函数的地址
//	std::function<int(int, int)> func3 = std::bind(&Sub::sub, s,
//		placeholders::_1, placeholders::_2);
//	// 参数调换顺序
//	std::function<int(int, int)> func4 = std::bind(&Sub::sub, s,
//		placeholders::_2, placeholders::_1);
//	cout << func3(1, 2) << endl;
//	cout << func4(1, 2) << endl;
//	return 0;
//}

