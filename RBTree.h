#pragma once
#include <assert.h>
enum Colour
{
	RED,
	BLACK
};

template <class T>
struct RBTreeNode
{	
	RBTreeNode<T>* _left;
	RBTreeNode<T>* _right;
	RBTreeNode<T>* _parent;
	Colour _col;
	T _data;

	RBTreeNode(const T& data)
		:_left(nullptr),
		_right(nullptr),
		_parent(nullptr),
		_col(RED),
		_data(data)
	{}

};


template<class T,class Ptr,class Ptr1>
struct RBIterator {

	typedef RBTreeNode<T> Node;
	typedef RBIterator<T,Ptr,Ptr1> self;
	
	RBIterator(Node* node)
		:_node(node)
	{}

	self& operator++()
	{
		Node* curRight = _node->_right;

		if (curRight)
		{
			while (curRight->_left)
			{
				curRight = curRight->_left;
			}
			_node = curRight;
		}
		else
		{
			Node* cur = _node;
			Node* parent = cur->_parent;
			while (parent&&cur == parent->_right)
			{
				cur = parent;
				parent = cur->_parent;
			}
			_node = parent;
		}
		return *this;
	}

	self& operator--()
	{
		Node* curLeft = _node->_left;
		if (curLeft)
		{
			while (curLeft->_right)
			{
				curLeft = curLeft->_right;
			}
			_node = curLeft;
		}
		else
		{
			Node* cur = _node;
			Node* parent = _node->_parent;

			while (parent && parent->_left == cur)
			{
				cur = parent;
				parent = parent->_parent;
			}
			_node = parent;
		}
		return *this;

	}

	Ptr operator*()
	{
		return _node->_data;
	}

	Ptr1 operator->()
	{
		return &(_node->_data);
	}

	bool operator==(const self& it)
	{
		return _node == it._node;
	}

	bool operator!=(const self& it)
	{
		return _node != it._node;
	}


private:
	Node* _node;
};

template <class K, class T,class Compare>
class RBTree
{
public:
	typedef RBTreeNode<T> Node;
	typedef RBIterator<T, T&, T*> iterator;
	typedef RBIterator<T, const T&, const T*> const_itertaor;

public:

	const_itertaor begin()const
	{
		if (_root == nullptr)
			return nullptr;
		Node* node = _root;
		while (node->_left)
		{
			node = node->_left;
		}
		return node;
	}

	const_itertaor end()const
	{
		return nullptr;
	}

	iterator begin()
	{
		if (_root == nullptr)
			return nullptr;
		Node* node = _root;
		while (node->_left)
		{
			node = node->_left;
		}
		return node;
	}

	iterator end()
	{
		return iterator(nullptr);
	}

	pair<iterator,bool> Insert(const T& data)
	{
		//1 寻找要插入的位置
		if (_root == nullptr)
		{
			_root = new Node(data);
			_root->_col = BLACK;
			return make_pair(iterator(_root),true);
		}
		Node* parent = _root;
		Node* cur = _root;
		Compare com;

		while (cur)
		{
			if (com(cur->_data) > com(data))
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (com(cur->_data) < com(data))
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{
				return make_pair(iterator(cur),false);
			}
			if (cur)
				cur->_parent = parent;
		}

		cur = new Node(data);
		if (com(data) > com(parent->_data))
		{
			parent->_right = cur;
		}
		else {
			parent->_left = cur;
		}
		cur->_parent = parent;
		Node* newnode = cur;
		//开始改变颜色
		while (parent && parent->_col == RED)
		{
			Node* grandfather = parent->_parent;

			//parent节点在祖父节点的左边
			if (parent == grandfather->_left)
			{
				//第一种情况，需要变色处理
				Node* uncle = grandfather->_right;
				if (uncle && uncle->_col == RED)
				{
					uncle->_col = BLACK;
					parent->_col = BLACK;
					grandfather->_col = RED;
					cur = grandfather;
					parent = cur->_parent;
				}
				else
				{
					//第二种情况 uncle节点为空或者是黑色节点
					if (cur == parent->_left)
					{
						RotateR(grandfather);
						parent->_col = BLACK;
						grandfather->_col = RED;
					}
					else
					{
						RotateL(parent);
						RotateR(grandfather);
						parent->_col = BLACK;
						grandfather->_col = RED;
					}
					//由于根节点变成了黑色，那就不用再往上判断了
					break;
				}
			}
			else
			{
				Node* uncle = grandfather->_left;
				if (uncle && uncle->_col == RED)
				{
					uncle->_col = BLACK;
					parent->_col = BLACK;
					grandfather->_col = RED;
					cur = grandfather;
					parent = cur->_parent;
				}
				else
				{
					//第二种情况 uncle节点为空或者是黑色节点
					if (cur == parent->_left)
					{
						RotateR(parent);
						RotateL(grandfather);
						parent->_col = BLACK;
						grandfather->_col = RED;
					}
					else
					{
						RotateL(grandfather);
						parent->_col = BLACK;
						grandfather->_col = RED;
					}
					//由于根节点变成了黑色，那就不用再往上判断了
					break;
				}
			}
		}
		//将根节点的颜色变成黑色就行了
		_root->_col = BLACK;

		return make_pair(iterator(cur),true);

	}

	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;

		subR->_left = parent;
		parent->_right = subRL;

		Node* ppnode = parent->_parent;

		if (subRL)
			subRL->_parent = parent;
		parent->_parent = subR;

		if (parent == _root)
		{
			_root = subR;
		}
		else if (parent == ppnode->_left)
		{
			ppnode->_left = subR;
		}
		else if (parent == ppnode->_right)
		{
			ppnode->_right = subR;
		}
		subR->_parent = ppnode;
	}

	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		subL->_right = parent;
		parent->_left = subLR;

		Node* ppnode = parent->_parent;

		parent->_parent = subL;
		if (subLR)
			subLR->_parent = parent;
		if (parent == _root)
		{
			_root = subL;
		}
		else if (parent == ppnode->_left)
		{
			ppnode->_left = subL;
		}
		else if (parent == ppnode->_right)
		{
			ppnode->_right = subL;
		}

		subL->_parent = ppnode;
	}

	void Inorder()
	{
		_Inorder(_root);
	}



	bool IsRBTree()
	{
		if (_root == nullptr)
			return true;
		Node* tmp = _root;
		int Countnum = 0;
		while (tmp)
		{
			if (tmp->_col == BLACK)
			{
				Countnum++;
			}
			tmp = tmp->_left;
		}
		return check(_root, 0, Countnum);
	}

private:
	bool check(Node* root, int comparenum, int  Countnum)
	{
		if (root == nullptr)
		{
			if (Countnum != comparenum)
				return false;
			return true;
		}

		if (root->_col == BLACK)
			comparenum++;
		if (root->_parent)
			if (root->_col == RED && root->_parent->_col == root->_col)
				return false;

		return check(root->_left, comparenum, Countnum) && check(root->_right, comparenum, Countnum);
	}

	void _Inorder(Node* root)
	{
		if (root == nullptr)
			return;
		_Inorder(root->_left);
		cout << root->_kv.first << endl;
		_Inorder(root->_right);
	}

	Node* _root = nullptr;
};
