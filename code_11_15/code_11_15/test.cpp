#include <iostream>

using namespace std;


template <typename T>

//模版
void Swap(T& x, T& y)
{
	T tmp = x;
	x = y;
	y = tmp;
}
//int main()
//{
//	int a = 10;
//	double b = 20.0;
//
//	//Swap<int>(a,(int) b);
//
//	return 0;
//}

class Date {
private:
	int _year;
	int _month;
	int _day;

public:
	////友元函数的声明
	//friend void func(Date& tmp);
	Date(int year = 2023, int month = 11, int day = 22)
	{
		_year = year;
		_month = month;
		_day = day;
	}

	void print() {
		cout << _year << " " << _month << " " << _day<<endl;
	}

	friend ostream& operator<<(ostream& _cout, Date& d);

	friend istream& operator>>(istream& _cin, Date& d);
};

ostream& operator<<(ostream& _cout, Date& d)
{
	_cout << d._year << " " << d._month << " " << d._day << endl;
	return _cout;
}

istream& operator>>(istream& _cin, Date& d)
{
	_cin >> d._year;
	_cin >> d._month;
	_cin >> d._day;

	return _cin;
}

////友元函数的定义
//void func(Date& tmp)
//{
//	tmp._year = 2023;
//	tmp._month = 11;
//	tmp._day = 15;
//}



//int main()
//{
//	Date d1;
//	//func(d1);
//	
//	d1.print();
//
//
//	return 0;
//}


int main()
{
	Date d1;
	cout << d1;

	cin >> d1;
	cout << d1;
	
	return 0;
}


