#include <iostream>

using namespace std;

//int global_date = 1;
//static int n = 1;
//
//void test()
//{
//
//}


//int main()
//{
//	int* b1 = new int;    //申请一个int的空间
//	int* b2 = new int[5]; //申请5个int的空间，就是一个数组
//
//	int* b3 = new int(10); //开辟一个int空间，用10去进行初始化
//	int* b4 = new int[5] {1, 2, 3}; //开辟5个int空间，用{}进行初始化
//
//	delete b1;
//	delete[] b2;
//	delete b3;
//	delete[] b4;
//	return 0;
//}


//
//int main()
//{
//	int* p1 = new int(10); //开辟一个int空间，结合（）中的内容进行初始化
//	int* p2 = new int[5] {1, 2, 3}; //开辟5个连续的int空间，结合{}中的内容进行初始化
//
//	delete p1;
//	delete[] p2;
//	return 0;
//}


//class Date {
//private:
//	int _year;
//	int _month;
//	int _day;
//
//public:
//	Date(int year = 2023 , int month = 11, int day = 21)
//	{
//		cout << "Date 构造函数调用了" << endl;
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	~Date(){
//		cout << "析构函数被调用了" << endl;
//	}
//};
//
//int main()
//{
//		Date* d1 = new Date(2023, 11, 21);
//		Date* d2 = new Date[5]{ Date(2023,11,20) };//初始化一部分，其他的是默认构造初始化，如果没有默认构造也会报错
//		Date* d3 = new Date;//调用默认构造，无默认构造就会报错
//
//		delete d1;
//		delete[] d2;
//		delete d3;
//		return 0;
//	
//	return 0;
//}


//class Stack {
//private:
//	int* p;
//	int _capacity;
//	int _top;
//
//public:
//	Stack(int capacity = 4)
//	{
//		cout << "Stack(int capacity = 4)" << endl;
//		p = new int[capacity];
//		_capacity = capacity;
//		_top = 0;
//	}
//
//	~Stack()
//	{
//		cout << "~Stack()" << endl;
//		delete[] p;
//		_capacity = _top = 0;
//		p = nullptr;
//	}
//
//};
//
//int main()
//{
//	Stack* st1 = new Stack;
//	Stack* st2 = (Stack*)operator new(sizeof(Stack));
//	return 0;
//}



//定位new的使用

//class A {
//private:
//	int _a;
//	int _b;
//
//public:
//
//	A(int a = 10)
//	{
//		cout <<"A(int a = 10)"<< endl;
//		_a = a;
//	}
//
//	A(int a , int b )
//	{
//		cout << "A(int a = 1, int b = 2)" << endl;
//	}
//
//	~A()
//	{
//		cout << "~A()" << endl;
//	}
//};
//
//int main()
//{
//	A* a1 = (A*)operator new(sizeof(A));//开辟对象的空间
//	new(a1)A;//定位new,显式的调用构造函数，调用默认构造
//	a1->~A();//显式的调用析构函数
//	free(a1);//释放对象空间
//
//	A* a2 = (A*)operator new(sizeof(A));
//	new(a2)A(10, 20);//定位new，调用两个参数的构造函数
//	a2->~A();
//	free(a2);
//}

//class Time {
//	friend class Date;//声明日期类就是时间类的友元类！在日期类中就可以访问到时间类的私有成员
//private:
//	int _hour;
//	int _minute;
//	int _second;
//
//public:
//	Time(int hour = 0,int minute = 0,int second = 0)
//		:_hour(hour),
//		_minute(minute),
//		_second(second)
//	{}
//};
//
//class Date {
//private:
//	int _year;
//	int _month;
//	int _day;
//	Time _t;
//
//public:
//	void SetTimeofDate(int hour, int minute, int second) {
//		_t._hour = hour;
//		_t._minute = minute;
//		_t._second = second;
//	}
//
//};

class A {

};

class B {
	friend class A;
};

class C {
	friend class B;
};

