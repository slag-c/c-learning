#pragma once


#include<vector>
template<class K,class V>
struct HashNode
{
	typedef HashNode<K, V> Node;
	Node* _next;
	pair<K, V> _kv;

	HashNode(const pair<K,V>& kv)
		:_next(nullptr),
		_kv(kv)
	{}
};


template<class K, class V>
class HashTable {
	typedef HashNode<K, V> Node;
public:
	HashTable(size_t size = 10)
	{
		_table.resize(size,nullptr);
	}
    ~HashTable（）
    {
        for(int i = 0;i<_table.size();i++)
        {
            Node* cur = _table[i];
            while(cur)
            {
                Node* next = cur->_next;
                delete cur;
                cur = next;
            }
        }
    }
	bool insert(const pair<K, V>& kv)
	{
		//判断是否要进行扩容处理
		//当负载因子为1时，进行扩容处理
		if (num == _table.size())
		{
			HashTable<K, V> h1(_table.size() * 2);
			//遍历一遍
			for (int i = 0; i < _table.size(); i++)
			{
				Node* cur = _table[i];
				while (cur)
				{
					Node* next = cur->_next;
					size_t hashi = cur->_kv.first % h1._table.size();
					//把节点移动过去
					cur->_next = h1._table[hashi];
					h1._table[hashi] = cur;
					cur = next;
				}
				_table[i] = nullptr;
			}
			_table.swap(h1._table);
		}
		//插入逻辑
		size_t hashi = kv.first % _table.size();
		Node* node1 = new Node(kv);
		node1->_next = _table[hashi];
		_table[hashi] = node1;
		num++;
		return true;
	}

	Node* Find(const K& key)
	{
		size_t hashi = key % _table.size();
		Node* cur = _table[hashi];
		while (cur)
		{
			if (cur->_kv.first == key)
			{
				return cur;
			}
			cur = cur->_next;
		}
		return nullptr;
	}

	bool erase(const K& key)
	{
		size_t hashi = key % _table.size();
		Node* parent = nullptr;
		Node* cur = _table[hashi];
		if (cur->_next == nullptr)
		{
			delete cur;
			_table[hashi] = nullptr;
			return true;
		}
		else {
			while (cur)
			{
				if (cur->_kv.first == key)
				{
					parent->_next = cur->_next;
					delete cur;
					return true;
				}
				parent = cur;
				cur = cur->_next;
			}
		}
		return false;
	}

private:
	vector<Node*> _table;
	size_t num = 0;
};