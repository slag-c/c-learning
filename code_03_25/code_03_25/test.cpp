#include <iostream>

using namespace std;

#include <vector>




int main()
{

	vector<int> v;
	v.push_back(5);
	v.push_back(4);

	vector<int>::iterator it = v.begin();

	while (it!=v.end())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;
	return 0;

}