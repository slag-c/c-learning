#pragma once
#include<vector>
enum State {
	EMPTY,
	DELETE,
	EXIST
};

template<class K,class V>
struct HashData {
	pair<K, V> _kv;
	State _state;
};

template<class K>
struct com {
	size_t operator()(const K& key)
	{
		return (size_t)key;
	}
};


template<>
struct com<string> {
	size_t operator()(const string& s1)
	{
		size_t sum = 0;
		for (auto e : s1)
		{
			sum += e;
			sum *= 131;
		}
		return sum;
	}
};
template<class K,class V,class Com = com<K>>
class HashMap {
public:
	HashMap(size_t size = 10)
	{
		_vtable.resize(size);
	}
	

	bool Insert(pair<K, V> kv)
	{
		if ((double)num / _vtable.size() >= 0.7)
		{
			//需要进行扩容处理
			HashMap<K,V,Com> h1(_vtable.size() * 2);
			//遍历旧表数据，插入到新表中
			for (int i = 0; i < _vtable.size(); i++)
			{
				if (_vtable[i]._state == EXIST)
				{
					h1.Insert(_vtable[i]._kv);
				}
			}
			std::swap(_vtable, h1._vtable);
		}
		Com com;
		size_t x = com(kv.first) % _vtable.size();
		while (_vtable[x]._state == EXIST)
		{
			x++;
			x = x % _vtable.size();
		}
		_vtable[x]._kv = kv;
		_vtable[x]._state = EXIST;
		num++;
		return true;
	}
	// 查找
	size_t Find(const K& key)
	{
		Com com;
		size_t x = com(key) % _vtable.size();
		while (_vtable[x]._state != EMPTY)
		{
			if (_vtable[x]._kv.first == key)
			{
				return x;
			}
			else {
				x++;
				x = com(x) % _vtable.size();
			}
		}
		return _vtable.size();
	}

	// 删除
	bool Erase(const K& key)
	{
		size_t x = Find(key);
		if (x == _vtable.size())
		{
			return false;
		}
		_vtable[x]._state = DELETE;
		num--;
		return true;
	}

	bool empty()
	{
		return num==0;
	}

private:
	vector<HashData<K, V>> _vtable;
	size_t num;
};
