#define  _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <list>

using namespace std;


//int main()
//{
//	// 左值
//	int a = 3;
//	int& b = a;
//	const int& c = move(6);
//	//int&& c = move(a);
//	//一切皆可以列表化
//
//	//int a{ 10 };
//	list<string> l1;
//
//	int&& tmp = 10 ;
//	tmp++;
//	cout << tmp << endl;
//
//	return 0;
//}

//
//int main()
//{
//	// the type of il is an initializer_list 
//	auto il = { 10, 20, 30 };
//	cout << typeid(il).name() << endl;
//	return 0;
//}


//int main()
//{
//	int a = 10;
//	//内置类型利用初始化列表初始
//	int b = { 10 };
//	int c{ 10 };
//}

#include <string>
class Student {
private:
	int _age;
	string _sex;
	string _classid;
public:
	Student(int age = 0,string sex = "男",string classid = "2024")
		:_age(age),
		_sex(sex),
		_classid(classid)
	{}
};

//int main()
//{
//	//自定义类型支持列表化
//	Student s1 = { 18,"男","1234" };
//	Student s2{ 19,"女","1234" };
//	Student* stu = new Student[4]{ { 19,"女","1234" },{ 18,"男","1234" } };
//	return 0;
//}

#include <vector>
//int main()
//{
//	//不同长度的对象
//	vector<int> v1 = { 1,2,3 };
//	vector<int> v2 = { 1,2,3,4 };
//	return 0;
//}


//int main()
//{
//	//以下a b *b c是左值
//	int a = 3;
//	int* b = new int(20);
//	const int c = 10;
//
//	//以下是对左值的引用
//	int& ya = a;
//	int* yb1 = b;
//	const int& yc = c;
//	int& yb2 = *b;
//	return 0;
//}



//int main()
//{
//	int b = 10;
//	const int& c = 10;
//
//
//	int&& c = b;
//	int&& d = move(b);
//}


#include"String.h"

int main()
{

	return 0;
}

