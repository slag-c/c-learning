#pragma once


#include <assert.h>

using namespace std;
namespace demo
{
	//节点类
	template<class T>
	struct Listnode {
		Listnode<T>* _prev;
		Listnode<T>* _next;
		T _val;
		Listnode(const T& val = T())
			:_prev(nullptr),
			_next(nullptr),
			_val(val)
		{}
	};

	//迭代器类
	template<class T,class ptr>
	class _list_iterator {
	public:
		typedef Listnode<T> Node;
		typedef _list_iterator<T,ptr> self;
		Node* _node;
	

		_list_iterator(Node* ptr)
			:_node(ptr)
		{}


		
		ptr operator*()
		{
			return _node->_val;
		}

		//前置++
		self& operator++()
		{
			_node = _node->_next;
			return *this;
		}

		//后置++
		self operator++(int)
		{
			self tmp(*this);
			_node = _node->_next;
			return tmp;
		}

		//前置--
		self& operator--()
		{
			_node = _node->prev;
			return *this;
		}

		//后置--

		self operator--(int)
		{
			self tmp(*this);
			_node = _node->prev;
			return tmp;
		}

		T* operator->()
		{
			return &_node->_val;
		}

		bool operator!=(const self& t)
		{
			return _node != t._node;
		}

		bool operator==(const self& t)
		{
			return _node == t._node;
		}
	};


	//链表类
	template <class T>
	class list
	{
	public:
		typedef Listnode<T> Node;
		typedef _list_iterator<T,T&> iterator;
		typedef _list_iterator<T, const T&> const_iterator;

		const_iterator begin()const
		{
			return _head->_next;
		}

		const_iterator end()const
		{
			return _head;
		}

		iterator begin()
		{
			return _head->_next;
		}

		iterator end()
		{
			return _head;
		}

		void empty_init()
		{
			_head = new Node;
			_head->_next = _head;
			_head->_prev = _head;
		}
		list()
		{
			empty_init();
		}

		//拷贝构造
		list(list<T>& lt)
		{
			empty_init();
			iterator it = begin();
			while (it!=end())
			{
				push_back(it._node->_val);
				it++;
			}
		}

		//尾部插入
		void push_back(const T& x)
		{
			insert(end(), x);
		}


		//尾部删除
		void pop_back()
		{
			erase(--end());
		}

		//头插
		void push_front(const T& x)
		{
			insert(begin(), x);
		}

		//头删
		void pop_front()
		{
			erase(begin());
		}

		void swap(list<T>& it)
		{
			std::swap(_head, it._head);
		}

		void clear()
		{
			iterator it = begin();
			while (it!= end())
			{
				it = erase(it);
			}
		}

		//在pos位置进行插入
		iterator insert(iterator pos,const T& x)
		{
			Node* node1 = new Node(x);
			Node* node2 = pos._node;
			
			node1->_next = node2;
			node2->_prev->_next = node1;

			node1->_prev = node2->_prev;
			node2->_prev = node1;

			return node1;
		}


		//删除pos位置的节点，并且返回删除掉的下一个节点位置
		iterator erase(iterator pos)
		{
			assert(pos != end());
			Node* tmp = pos._node;
			Node* node1 = tmp->_next;

			tmp->_prev->_next = node1;
			node1->_prev = tmp->_prev;
			
			delete tmp;
			return node1;
		}

		//赋值函数
		list<T>& operator=(list<T> lt)
		{
			swab(lt);
			return *this;

		}

		//析构函数
		~list()
		{
			clear();
			delete _head;
			_head = nullptr;
		}
	private:
		Node*  _head;
	};
}
