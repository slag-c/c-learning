#pragma once

#include "RBTree.h"



template<class K>
class Set {
	//�麯��
	struct SetCompare {
		const K& operator()(const K& data)
		{
			return data;
		}
	};
	typedef typename RBTree < K, const K, SetCompare>::iterator iterator;
	typedef typename RBTree < K, const K, SetCompare>::const_itertaor const_iterator;
public:

	iterator begin()
	{
		return _t.begin();
	}
	iterator end()
	{
		return _t.end();
	}
	const_iterator begin()const 
	{
		return _t.begin();
	}
	const_iterator end()const 
	{
		return _t.end();
	}

	pair<iterator,bool> insert(const K& key)
	{
		pair<iterator, bool> it = _t.Insert(key);
		return it;
	}
private:
	RBTree<K,const K,SetCompare> _t;
};

void testSet()
{
	Set<int> S1;
	int a[] = { 1,3,24,43,13 };
	for (auto e : a)
	{
		S1.insert(e);
	}

	auto it = S1.begin();

	while (it != S1.end())
	{
		cout << *it << endl;
		++it;
	}
}


