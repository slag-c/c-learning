#include <iostream>

using namespace std;

//void Swap(int& a, int& b)
//{
//	int tmp = a;
//	a = b;
//	b = tmp;
//}

template<typename T>

void Swap(T& x,T& y)
{
	T tmp = x;
	x = y;
	y = tmp;
}

//int main()
//{
//	int a = 10;
//	int b = 20;
//	Swap(a,b);
//	char x = '0';
//	char y = '9';
//	Swap(x, y);
//	return 0;
//}

int main(void)
{
	int a = 10;
	double b = 20.0;
	
	//Swap<int>(a,b);

	return 0;
}