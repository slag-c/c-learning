#pragma once
#include<string>
#include<bitset>

struct HashFuncBKDR
{
	// BKDR
	size_t operator()(const string& s)
	{
		size_t hash = 0;
		for (auto ch : s)
		{
			hash *= 131;
			hash += ch;
		}

		return hash;
	}
};

struct HashFuncAP
{
	// AP
	size_t operator()(const string& s)
	{
		size_t hash = 0;
		for (size_t i = 0; i < s.size(); i++)
		{
			if ((i & 1) == 0) // 偶数位字符
			{
				hash ^= ((hash << 7) ^ (s[i]) ^ (hash >> 3));
			}
			else              // 奇数位字符
			{
				hash ^= (~((hash << 11) ^ (s[i]) ^ (hash >> 5)));
			}
		}

		return hash;
	}
};

struct HashFuncDJB
{
	// DJB
	size_t operator()(const string& s)
	{
		size_t hash = 5381;
		for (auto ch : s)
		{
			hash = hash * 33 ^ ch;
		}

		return hash;
	}
};

//使用了3个仿函数，将其映射到3个不同的位置
template<size_t N,
	class T = string,
	class HashFunc1 = HashFuncBKDR,
	class HashFunc2 = HashFuncAP,
	class HashFunc3 = HashFuncDJB>
class BLoomFiler {
public:
	void set(const T& data)
	{
		size_t hash1 = HashFunc1()(data) % M;
		size_t hash2 = HashFunc2()(data) % M;
		size_t hash3 = HashFunc3()(data) % M;

		_bs.set(hash1);
		_bs.set(hash2);
		_bs.set(hash3);
	}

	bool test(const T& data)
	{
		size_t hash1 = HashFunc1()(data) % M;
		if (_bs.test(hash1) == false)
		{
			return false;
		}
		size_t hash2 = HashFunc2()(data) % M;
		if (_bs.test(hash2) == false)
		{
			return false;
		}
		size_t hash3 = HashFunc3()(data) % M;
		if (_bs.test(hash3) == false)
		{
			return false;
		}
		return true;
	
	}
private:

	static const size_t M = 10 * N;

	bitset<M> _bs;
};

void TestBloomFilter1()
{
	string strs[] = { "百度","字节","腾讯" };
	BLoomFiler<10> bf;
	for (auto& s : strs)
	{
		bf.set(s);
	}

	for (auto& s : strs)
	{
		cout << bf.test(s) << endl;
	}

	for (auto& s : strs)
	{
		cout << bf.test(s + 'a') << endl;
	}

	cout << bf.test("摆渡") << endl;
	cout << bf.test("百渡") << endl;
}
