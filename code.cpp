#include<iostream>
using namespace std;
#include <memory>

////C++98的写法
//class B {
//public:
//	B() {};
//	
//private:
//	B(const B& b);
//	int _b;
//};
//
//
////C++11之后的写法
//class A {
//public:
//	A() {}
//	A(const A& p) = delete;
//private:
//	int _a = 0;
//};

//int main()
//{
//	A a1;
//	A a2(a1);
//	B b1;
//	B b2(b2);
//	return 0;
//}


//class HeapCreat {
//public:
//	static HeapCreat* Creatable()
//	{
//		return new HeapCreat;
//	}
//
//	HeapCreat(const HeapCreat& heap) = delete;
//	HeapCreat& operator=(const HeapCreat& heap) = delete;
//private:
//	HeapCreat() {};
//	int _a;
//};
//
//
//int main()
//{
//	HeapCreat* p = HeapCreat::Creatable();
//	return 0;
//}

//class StackCreat {
//public:
//	static StackCreat Creatable()
//	{
//		return StackCreat();
//	}
//
//	StackCreat(const StackCreat& heap) = delete;
//	StackCreat& operator=(const StackCreat& heap) = delete;
//private:
//	StackCreat() {};
//	int _a;
//};
//
//
//int main()
//{
//	StackCreat::Creatable();
//	return 0;
//}

//class StackCreat {
//public:
//
//	void* operator new(size_t size);//重载全局operator new
//	StackCreat(const StackCreat& heap) = delete;
//	StackCreat& operator=(const StackCreat& heap) = delete;
//	StackCreat() {};
//	int _a;
//};
//
//int main()
//{
//	StackCreat s1;
//	//StackCreat a2 = new StackCreat; error
//	return 0;
//}

//class HeapCreat {
//public:
//	HeapCreat() {};
//
//	HeapCreat(const HeapCreat& heap) = delete;
//	HeapCreat& operator=(const HeapCreat& heap) = delete;
//	void destory()
//	{
//		delete this;
//	}
//private:
//	~HeapCreat() {};
//	int _a;
//};
//
//
//int main()
//{
//	HeapCreat* ptr = new HeapCreat;
//	//delete ptr 调不动，因为析构被封，必须通过内部来
//	ptr->destory();	
//	//通过智能指针来删除
//	//shared_ptr<HeapCreat> ptr(new HeapCreat, [](HeapCreat* p) {p->destory(); });
//	return 0;
//}


//class A final {};
//
//class B :public A {
//
//};

#include <string>
////饿汉模式的实现
//class SingLeon {
//public:
//	//获得实例的位置
//	static  SingLeon* getInstacne()
//	{
//		return &_sint;
//	}
//private:
//	SingLeon(int a = 0,string b = "")
//		:_a(a),
//		_b(b)
//	{}
//
//	int _a;
//	string _b;
//	//声明在类中，定义在类外，属于静态区，全局变量
//	static SingLeon _sint;
//};
////在类外进行定义
//SingLeon SingLeon::_sint(1,"a");
//
////饿汉模式  进入main函数之前就已经实例化好了
//int main()
//{
//	SingLeon* p1 = SingLeon::getInstacne();
//	SingLeon* p2 = SingLeon::getInstacne();
//	cout << p1 << endl;
//	cout << p2 << endl;
//	return 0;
//}


//
////懒汉模式的实现
//class SingLeon {
//public:
//	//获得实例的位置
//	static  SingLeon* getInstacne()
//	{
//		if (_sint==nullptr)
//		{
//			_sint = new SingLeon(1, "a");
//		}
//		return _sint;
//	}
//private:
//	SingLeon(int a = 0, string b = "")
//		:_a(a),
//		_b(b)
//	{}
//
//	int _a;
//	string _b;
//	//声明在类中，定义在类外，属于静态区，全局变量
//	static SingLeon* _sint;
//};
//SingLeon* SingLeon::_sint = nullptr;
//
//int main()
//{
//		SingLeon* p1 = SingLeon::getInstacne();
//		SingLeon* p2 = SingLeon::getInstacne();
//		cout << p1 << endl;
//		cout << p2 << endl;
//}




//int main()
//{
//	int a = 10;
//	float e = 1.0;
//	bool b = true;
//	char ch = 'a';
//	float* d = &e;
//	//1 整型与布尔型
//	a = b;
//	//2 整形字符型
//	ch = a;
//	//3 指针与整形 因为指针也是有大小的
//	d = (float*)a;
//	//4 不同类型的指针
//	d = (float*)&a;
//	return 0;
//}


//class String {
//public:
//	//单参数的默认构造，隐式类型转换
//	String(const char* ptr = "")
//	{}
//
//	operator int()
//	{
//		return 0;
//	}
//};
//
//int main()
//{
//	/*const char* ptr = "1111";
//	String s1(ptr);*/
//	String s1;
//	int b = s1;
//	return 0;
//}

//
//int main()
//{
//	int a = 10;
//	float e = 1.0;
//	bool b = true;
//	char ch = 'a';
//	float* d = &e;
//	//1 整型与布尔型
//	a = static_cast<int>(b);
//	//2 整形字符型
//	ch = static_cast<char>(a);
//	//3 指针与整形 因为指针也是有大小的
//	d = reinterpret_cast<float*>(a);
//	//4 不同类型的指针
//	d = reinterpret_cast<float*>(&a);
//	return 0;
//}


//int main()
//{
//	const int a = 1;
//	int* p = const_cast<int*>(&a);
//
//	*p = 3;
//
//	return 0;
//}


class A {
	virtual void test()
	{
		cout << "class A " << endl;
	}
public:
	int _a = 1;
};

class B : public A{
	virtual void test()
	{
		cout << "class B" << endl;
	}
public:
	int _b = 2;
};

void func(A* ptr)
{
	//父类指针转子类指针
	//如果检查到ptr传过来时，是父类的指针，返回0
	//从而保证不会产生访问越界问题
	B* b1 = dynamic_cast<B*>(ptr);
	if (b1) {
		cout << b1->_a << endl;
		cout << b1->_b << endl;
	}
	
}

int main()
{
	A a;
	B b;
	func(&a);
	func(&b);
}