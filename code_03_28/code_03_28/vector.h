#pragma once

//模拟实现vector类，并完成测试

namespace bit

{

    template<class T>

    class vector

    {

    public:

        // Vector的迭代器是一个原生指针

            typedef T* iterator；

            typedef const T* const_iterator；

            iterator begin()
            {
                return _start;
            }

            iterator end()
            {
                return _finish;
            }

            const_iterator cbegin()
            {
                return _start;
            }

            const_iterator cend() const
            {
                return _finish;
            }



            // construct and destroy

            vector()
                :_start = nullptr;
                 _finish = nullptr;
                 _endOfStorage = nullptr;
            {}

            vector(int n, const T& value = T())；

            template<class InputIterator>

        vector(InputIterator first, InputIterator last)；

            vector(const vector<T>& v)；

            vector<T>& operator= (vector<T> v)；

        ~vector()
        {
            if (_start)
            {
                _start = _finish = _endOfStorage = nullptr;
            }
        }

        // capacity
        size_t size() const
        {
            return _start - _finish;
        }

        size_t capacity() const
        {
            return _endOfStorage - _start;

        }

        void reserve(size_t n)
        {
            if (n > capacity())
            {
                size_t tmp = size();
                T* tmp = new T[n];
                if (_start) {
                    memcpy(_start, _tmp, tmp * sizeof(T));
                    delete[] _start;
                }
                _start = tmp;
                _finish = _start + tmp;
                _endOfStorage = _start + n;
            }
        }

            void resize(size_t n, const T& value = T())；



            ///////////////access///////////////////////////////

             T& operator[](size_t pos)
            {
                return _start[pos];

            }

            const T& operator[](size_t pos)const
            {
                return _start[pos];
            }



            ///////////////modify/////////////////////////////

            void push_back(const T& x)
            {

            }

            void pop_back()
            {
                _finish--;
            }

            void swap(vector<T>& v)；

            iterator insert(iterator pos, const T& x)；

            iterator erase(Iterator pos)；

    private:

        iterator _start; // 指向数据块的开始

        iterator _finish; // 指向有效数据的尾

        iterator _endOfStorage; // 指向存储容量的尾

    };

}
