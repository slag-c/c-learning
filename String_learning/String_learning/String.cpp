
#include <string.h>
#include <assert.h>
#include <iostream>
using namespace std;
namespace test {
	class String {
	private:
		size_t _capacity;
		size_t _size;
		char* _str;
		static const size_t npos = -1;

	public:
		//构造函数
		String(const char* str = "")
		{
			_capacity = strlen(str);
			_size = _capacity;
			_str = new char[_capacity + 1];

			strcpy(_str, str);
		}
		//析构函数
		~String()
		{
			delete[] _str;
			_str = nullptr;
			_size = 0;
			_capacity = 0;
		}

		//迭代器
		typedef char* itertor;

		itertor begin()
		{
			return _str;
		}

		itertor end()
		{
			return _str + _size;
		}


		//重载[]
		char& operator[](size_t n)
		{
			return _str[n];
		}

		//扩容处理
		void reverse(size_t n)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];
				_capacity = n;
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;
			}
		}


		//拷贝构造 s1(s2)
		String(const String& s)
		{
			_str = new char[s._capacity + 1];
			_capacity = s._capacity;
			_size = s._size;
			strcpy(_str, s._str);
		}

		//返回字符串
		char* c_str()
		{
			return _str;
		}

		//赋值操作 s1 = s2
		String& operator=(String s)
		{
			char* tmp = new char[s._capacity + 1];
			_size = s._size;
			_capacity = s._capacity;
			delete[] _str;
			_str = tmp;

			strcpy(_str, s._str);

			return *this;
		}

		//尾部插入字符串
		void push_back(char ch)
		{

			//判断是否要进行扩容处理
			if (_size == _capacity)
			{
				size_t newCapacity = _capacity == 0 ? 4 : 2 * _capacity;
				reverse(newCapacity);
			}
			//开始插入
			_str[_size] = ch;
			_size++;
			_str[_size] = '\0';
		}

		//拼接单个字符
		String& operator+=(char ch)
		{
			push_back(ch);
			return *this;
		}


		//增加多个字符
		void append(const char* str)
		{
			size_t len = strlen(str);
			if (len + _size > _capacity)
			{
				//扩容处理
				reverse(len + _size);
			}
			strcpy(_str + _size, str);
		}


		//拼接多个字符
		String& operator+=(const char* str)
		{
			append(str);
			return *this;
		}

		//在pos位置插入字符ch
		void insert(size_t pos, char ch)
		{
			assert(pos < _size);
			int end = _size;
			while (end <= (int)pos)
			{
				_str[end + 1] = _str[end];
				end--;
			}
			_str[pos] = ch;
		}

		//在pos位置插入字符串
		void insert(size_t pos, const char* str)
		{
			assert(pos < _size);
			size_t len = strlen(str);
			//判断是否进行扩容处理
			if (len + _size > _capacity)
			{
				reverse(len+_size);
			}
			
			int end = _size;
			while (end >= (int)pos)
			{
				_str[end + len] = _str[end];
				end--;
			}
			//拷贝过去
			strncpy(_str + pos, str, len);
		}


		void erase(size_t pos, size_t len = npos)
		{
			assert(pos < _size);

			if (len == npos || pos + len > _capacity)
			{
				//从pos位置开始往后面删除
				_str[pos] = '\0';
				_size = pos;
			}
			else {
				strcpy(_str + pos,_str+pos+len);
				_size -= len;
			}
		}

		//交换
		void swap(String& s)
		{
			

		}
		size_t find(char ch, size_t pos = 0)
		{
			for (size_t i = pos; i <= _size; i++)
			{
				if (_str[pos] == ch)
				{
					return i;
				}
			}
			return npos;
		}
		
		size_t find(const char* str, size_t pos = 0)
		{
			assert(pos < _size);
			char* tmp = strstr(_str, str);
			return tmp - _str;
		}



		String substr(size_t pos = 0, size_t len = npos)
		{
			assert(pos < _size);
			
			if (len == npos || pos + len > _capacity)
			{
				size_t n = _capacity - pos;
				char* tmp = new char[n + 1];
				strcpy(tmp, _str + pos);
				String s1(tmp);
				return s1;
			}
			else
			{
				char* tmp = new char[len + 1];
				strncpy(tmp, _str + pos, len);
				String s1(tmp);
				return s1;
			}

			
		}



		void clear()
		{
			_str[0] = '\0';
			_size = 0;
		}

		void print_str(const char* tmp)
		{
			for (size_t i = 0; i < _size; i++)
			{
				cout << tmp[i] << " ";
			}
			cout << endl;
		}
	};

}