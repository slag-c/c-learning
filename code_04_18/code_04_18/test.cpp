#include <iostream>
using namespace std;
#include "stack.h"
#include <deque>
//int main()
//{
//	demo::stack<int> lt;
//	lt.push(2);
//	cout << lt.size() << endl;
//	while (!lt.empty())
//	{
//		cout <<lt.top() <<endl;
//		lt.pop();
//	}
//	return 0;
//}

int main()
{
	demo::priority_queue<int,deque<int>,less<int>> lt;
	lt.push(3);
	lt.push(9);
	lt.push(99);
	while (!lt.empty())
	{
		cout<<lt.top()<<" ";
		lt.pop();
	}
	return 0;
}