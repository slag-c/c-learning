#pragma once

#include "RBTree.h"


template<class K,class V>
class Map {
	//�º���
	struct MapCompare {
		const K& operator()(const pair<const K, V>& data)
		{
			return data.first;
		}
	};
public:
	typedef typename  RBTree<K,pair<const K,V>,MapCompare>::iterator iterator;
	typedef typename RBTree<K, pair<const K,V>, MapCompare>::const_itertaor const_iterator;
private:
	RBTree<K, pair<const K, V>, MapCompare> _t;

public:
	iterator begin()
	{
		return _t.begin();
	}
	iterator end()
	{
		return _t.end();
	}
	const_iterator begin()const 
	{
		return _t.begin();
	}
	const_iterator end()const
	{
		return _t.end();
	}
	pair<iterator,bool> insert(pair<K,V> kv)
	{
		pair<iterator, bool> it = _t.Insert(kv);
		return it;
	}

};
void testMap1()
{
	int a[] = {1,2,3};
	Map<int, int> M1;
	for (auto e : a)
	{
		M1.insert(make_pair(e, e));
	}
	Map<int, int>::iterator it = M1.begin();
	while (it != M1.end())
	{
		cout << (*it).first << ":" << it->second << endl;;
		++it;
	}
}
