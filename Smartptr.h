#pragma once

//不允许进行拷贝的智能指针
namespace SmartPtr {
	template<class T>
	class unique_ptr {
	public:
		//构造函数
		unique_ptr(T* ptr = nullptr)
			:_ptr(ptr)
		{}
		unique_ptr(unique_ptr<T>& p1) = delete;
		//赋值重载
		unique_ptr<T>& operator=(const unique_ptr<T>& p1) = delete;
		T operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
		~unique_ptr()
		{
			if (_ptr)
			{
				cout << "delete" << endl;
				delete _ptr;
			}
		}
	private:
		T* _ptr;//指针
	};

	//不C++98的智能指针
	template<class T>
	class auto_ptr {
	public:
		//构造函数
		auto_ptr(T* ptr)
			:_ptr(ptr)
		{}

		//拷贝构造，实现管理权限的转移
		auto_ptr(auto_ptr<T>& p1)
		{
			_ptr = p1._ptr;
			p1._ptr = nullptr;
		}
		//赋值重载
		auto_ptr<T>& operator=(const auto_ptr<T>& p1)
		{
			//不是给自己赋值的情况下
			if (this != &p1)
			{
				if (_ptr)
				{
					_ptr = p1._ptr;
					p1._ptr = nullptr;
				}
				return *this;
			}
		}
		T operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
		~auto_ptr()
		{
			if (_ptr)
			{
				cout << "delete" << endl;
				delete _ptr;
			}
		}
	private:
		T* _ptr;//指针
	};


	//引入引用计数操作
	template<class T>
	class shared_ptr {
	public:
		//构造函数
		shared_ptr(T* ptr)
			:_ptr(ptr),
			_num(new int(1))
		{}

		//拷贝构造，实现管理权限的转移
		//s1(s2)
		shared_ptr(shared_ptr<T>& p1)
		{
			_ptr = p1._ptr;
			_num = p1._num;
			(*_num)++;
		}
		//赋值重载
		shared_ptr<T>& operator=(const shared_ptr<T>& p1)
		{
			//不是给自己赋值的情况下
			if (p1._ptr != _ptr)
			{
				destory();
				_ptr = p1._ptr;
				_num = p1._num;
				(*_num)++;
			}
		}
		void destory()
		{
			if (--(*_num) == 0)
			{
				cout << "delete" << endl;
				delete _ptr;
				delete _num;
				_ptr = nullptr;
				_num = nullptr;
			}
		}

		T operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
		~shared_ptr()
		{
			destory();
		}
	private:
		T* _ptr;  //指针
		int* _num; //引用计数数组
	};
}

int div()
{
	int a, b;
	cin >> a >> b;
	if (b == 0)
		throw invalid_argument("除0错误");
	return a / b;
}
void Func()
{
	// 1、如果p1这里new 抛异常会如何？
	// 2、如果p2这里new 抛异常会如何？
	// 3、如果div调用这里又会抛异常会如何？
	int* p1 = new int;
	int* p2 = new int;
	cout << div() << endl;
	delete p1;
	delete p2;
}
int main()
{
	try
	{
		Func();
	}
	catch (exception& e)
	{
		cout << e.what() << endl;
	}
	return 0;
}