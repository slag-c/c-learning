#include <iostream>
#include <string>

using namespace std;

int main()
{
	string s1;
	string s2("hello world");
	string s3(3, 'x');
	reverse(s2.begin(),s2.end());
	string s4(s3);

	cout << s2 << endl;
	cout << s3 << endl;
	cout << s4 << endl;
	return 0;
}