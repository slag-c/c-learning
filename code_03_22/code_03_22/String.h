#pragma once

#include <iostream>
#include <assert.h>
using namespace std;

namespace test {
	class String {
	private:
		size_t _size = 0;
		size_t _capacity = 0;
		char* _str = nullptr;

		const static size_t npos = -1;

	public:
		typedef char* itertor;

		itertor begin()
		{
			return _str;
		}

		itertor end()
		{
			return _str + _size;
		}


		size_t size()
		{
			return _size;
		}

		const char* c_str() const
		{
			return _str;
		}

		//构造函数
		String(const char* str = "")
		{
			_size = strlen(str);
			_capacity = _size;
			_str = new char[_capacity + 1];

			//拷贝字符串
			strcpy(_str,str);
		}

		//拷贝构造 s1(s2)
		String(const String& s)
		{
			char* tmp = new char[s._capacity + 1];
			strcpy(tmp, s._str);
			_size = s._size;
			_capacity = s._capacity;
			delete[] _str;
			_str = tmp;
		}

		//赋值 s1 = s2
		String& operator=(const String& s)
		{

		}

		//析构函数
		~String() {
			delete[] _str;
			_str = nullptr;
			_size = 0;
			_capacity = 0;
		}


		char& operator[](size_t pos)
		{
			return _str[pos];
		}

		//扩容处理
		void reserve(size_t n)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n+1];
				strcpy(tmp, _str);
				size_t num1 = _size;
				delete[] _str;
				_str = tmp;
				_size = num1;
				_capacity = n;
			}
		}

		//从尾部插入一个字符
		void push_back(char ch)
		{
			if (_size == _capacity)
			{
				size_t newCapacity = _capacity == 0 ? 4 : 2 * _capacity;
				reserve(newCapacity);
			}

			_str[_size++] = ch;
			_str[_size] = '\0';
		}

		void append(const char* str)
		{
			size_t len = strlen(str);
			if (len + _size > _capacity)
			{
				reserve(len + _size);
			}
			strcpy(_str + _size, str);
			_size += len;
		}



		String& operator+=(char ch)
		{
			push_back(ch);
			return *this;
		}


		String& operator+=(const char* str)
		{
			append(str);
			return *this;
		}

		//在pos位置插入字符ch
		void insert(size_t pos, char ch)
		{
			assert(pos < _size);
			if (_size == _capacity)
			{
				size_t newCapacity = _capacity == 0 ? 4 : 2 * _capacity;
				reserve(newCapacity);
			}

			size_t end = _size + 1;
			while (end>pos)
			{
				_str[end] = _str[end - 1];
				end--;
			}

			_str[pos + 1] = _str[pos];
			_str[pos] = ch;
			_size++;
		}


		void insert(size_t pos, const char* str)
		{
			assert(pos < _size);
			size_t len = strlen(str);

			if (_size == _capacity || _size + len > _capacity)
			{
				reserve(_size + len);
			}

			strncpy(_str + pos, str, len);
		}


		void erase(size_t pos, size_t len = npos)
		{
			assert(pos < _size);

			if (len == npos || pos + len > _capacity)
			{
				_str[pos] = '\0';
				_size = pos;
			}
			else
			{
				strcpy(_str + pos, _str + pos + len);
				_size -= len;
			}
		}
		void swap(string& s)
		{

		}
		size_t find(char ch, size_t pos = 0)
		{
			for (size_t i = pos; i < _size; i++)
			{
				if (_str[i] == ch)
				{
					return i;
				}
			}
			//表示没有找到
			return npos;
		}

		size_t find(const char* str, size_t pos = 0)
		{
			char* ptr = strstr(_str+pos, str);
			if (ptr == nullptr)
			{
				return npos;
			}
			return ptr - _str;
		}
		String substr(size_t pos = 0, size_t len = npos)
		{
			assert(pos < _size);
			String s1;
			
			size_t end = pos+len;
			if (len == npos || len + pos > _size)
			{
				end = _size;
			}
			s1.reserve(end-pos);
			for (size_t i = pos; i < end; i++)
			{
				s1._str[i] = _str[i];
			}
			return s1;

		}
		void clear()
		{
			_str[0] = '\0';
			_size = 0;
		}


	};
}
