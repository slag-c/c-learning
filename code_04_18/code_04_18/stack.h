#pragma once

#include <deque>
#include <list>
namespace demo {
	template<class T,class Container = list<T>>
	class stack
	{
	public:
		void push(const T& x)
		{
			_con.push_back(x);
		}

		void pop()
		{
			_con.pop_back();
		}

		T& top()
		{
			return _con.top();
		}

		size_t size()
		{
			return _con.size();
		}

		bool empty()
		{
			return _con.empty();
		}
	private:
		Container _con;
	};

	template<class T>
	class less
	{
	public:
		bool operator()(T& x,T& y)
		{
			return x > y;
		}
	};

	template <class T>
	class greater
	{
	public:
		bool operator()(T& x, T& y)
		{
			return x < y;
		}
	};
	template<class T,class Container = deque<T>,class compare = less<T>>


	class priority_queue
	{
	public:

		void adjust_up(size_t child)
		{
			//大根堆为例
			size_t parent = (child - 1) / 2;
			while (child > 0)
			{
				if (com(_con[child],_con[parent]))
				{
					swap(_con[parent], _con[child]);
					child = parent;
					parent = (child - 1) / 2;
				}
				else {
					break;
				}
			}
		}

		void adjust_down(size_t parent)
		{
			size_t child = 2 * parent + 1;
			while (child<_con.size())
			{
				if (child + 1 < _con.size() && _con[child + 1] > _con[child])
				{
					child++;
				}
				if(com(_con[child], _con[parent]))
				{
					swap(_con[child], _con[parent]);
					parent = child;
					child = 2 * parent + 1;
				}
				else {
					break;
				}
			}
		}
		bool empty()
		{
			return _con.empty();
		}
		void push(const T& x)
		{
			_con.push_back(x);
			//堆向上调整
			adjust_up(_con.size()-1);
		}

		size_t size()
		{
			return _con.size();
		}
		
		void pop()
		{
			swap(_con[0], _con[_con.size()-1]);
			_con.pop_back();
			//堆进行向下调整
			adjust_down(0);
		}

		T& top()
		{
			return _con.front();
		}

	private:
		Container _con;
		compare com;
	};



}
