#pragma once
#include <assert.h>


namespace bit
{
	template<class T>
	struct ListNode
	{
		ListNode<T>* _next;
		ListNode<T>* _prev;
		T _data;

		ListNode(const T& x = T())
			:_next(nullptr),
			_prev(nullptr),
			_data(x)
		{}
	};

	template<class T>
	//迭代器类
	struct __list_iterator
	{
		typedef ListNode<T> Node;
		typedef __list_iterator<T> self;
		Node* _node;
		__list_iterator(Node* x)
			:_node(x)
		{}
		// ++it
		self& operator++()
		{
			_node = _node->_next;
			return *this;
		}
		// it++
		self operator++(int)
		{
			self tmp(*this);
			_node = _node->_next;
			return tmp;
		}

		//前置--
		self& operator--()
		{
			_node = _node->_prev;
			return *this;
		}
		//后置--
		self operator--(int)
		{
			self tmp(*this);
			_node = _node->_prev;
			return tmp;
		}


		T& operator*()
		{
			return _node->_data;
		}
		bool operator!=(const self& s)
		{
			return _node != s._node;
		}
		bool operator==(const self& s)
		{
			return _node == s._node;
		}
	};

	template<class T>
	class list
	{
		typedef ListNode<T> Node;
	public:
		typedef __list_iterator<T> iterator;
		iterator begin()
		{
			return _head->_next;
		}
		iterator end()
		{
			return _head;
		}
		void empty_init()
		{
			_head = new Node;
			_head->_prev = _head;
			_head->_next = _head;
		}
		//构造函数
		list()
		{
			empty_init();
		}
		void clear()
		{
			iterator it = begin();
			while (it!=end())
			{
				it = erase(it);
			}
		}
		~list()
		{
			clear();
			delete _head;
			_head = nullptr;
		}

		//拷贝构造
		list(list<T>& lt)
		{
			_head = empty_init();
			iterator it = lt.begin();
			while (it != end())
			{
				push_back(it._node->_data);
			}
		}
		void swap(list<T>& tmp)
		{
			std::swap(_head, tmp->_head);
		}

		//赋值重载
		list<T>& operator=(list<T> lt)
		{
			swap(lt);
			return *this;
		}

		//尾插
		void push_back(const T& x)
		{
			insert(end(), x);
		}
		//头插
		void push_front(const T& x)
		{
			insert(begin(), x);
		}

		//尾删
		void pop_back()
		{
			erase(--end());
		}

		//头删
		void pop_front()
		{
			erase(begin());
		}

		//在pos位置插入
		iterator insert(iterator pos, const T& x)
		{
			Node* node1 = pos._node;
			Node* newnode = new Node(x);
			Node* node2 = node1->_prev;
			
			newnode->_next = node1;
			newnode->_prev = node2;

			node1->_prev = newnode;
			node2->_next = newnode;

			return newnode;
		}

		//删除在pos位置的节点
		iterator erase(iterator pos)
		{
			assert(pos != end());
			
			Node* node1 = pos._node;
			Node* node2 = node1->_prev;

			node2->_next = node1->_next;
			node1->_next->_prev = node2;

			delete node1;
			node1 = nullptr;

			return node2->_next;
		}

	private:
		Node* _head;
	};


	void test1()
	{
		list<int> st;
		st.push_back(2);
		st.push_back(3);
		st.push_front(1);
		st.push_back(2);

		for (auto ch : st)
		{
			cout << ch << " ";
		}
		cout << endl;

		list<int>::iterator it = st.begin();

		it = st.erase(it);
		it = st.erase(it);
		it = st.erase(it);
		it = st.erase(it);
		for (auto ch : st)
		{
			cout << ch << " ";
		}
		
	}
}