#pragma once

#include<vector>
template<size_t N>
class MyBitSet {
public:
	MyBitSet()
	{
		v1.resize(N/32+1,0);
	}

	
	void set(size_t x)
	{
		size_t i = x / 32;
		size_t j = x % 32;
		v1[i] |= (1 << j);
	}

	
	void reset(size_t x)
	{
		size_t i = x / 32;
		size_t j = x % 32;

		v1[i] &= ~(1 << j);

	}

	bool test(size_t x)
	{
		size_t i = x / 32;
		size_t j = x % 32;
		return v1[i] & (1 << j);
	}

private:
	vector<int> v1;
};



// 1. 给定100亿个整数，设计算法找到只出现一次的整数？
template<size_t N>
class BitOne {
public:
	void set(size_t x)
	{
		if (b1.test(x) == false && b2.test(x) == false)
		{
			b1.set(x);
		}
		else if (b1.test(x) == true && b2.test(x) == false)
		{
			//b2b1: 01->10 表示出现了两次
			b1.reset(x);
			b2.set(x);
		}
	}

	bool test(size_t x)
	{
		if (b1.test(x) == true)
		{
			return true;
		}
		return false;
	}

private:
	MyBitSet<N> b1;
	MyBitSet<N> b2;
};

void test1()
{
	int a[] = { 1,2,3,3,2,3,1432,1234,13,2134,13,1234,21,12,1 };
	BitOne<5000> testone;
	for (auto e : a)
	{
		testone.set(e);
	}
	for (int i = 0; i < 5000; i++)
	{
		if (testone.test(i))
		{
			cout << i << "只出现一次" << endl;
		}
	}
}

//2. 给两个文件，分别有100亿个整数，我们只有1G内存，如何找到两个文件交集？
void test2()
{
	MyBitSet<100> test1;
	MyBitSet<100> test2;

	int a1[] = { 2,31,52,65,12,43,54,64 };
	int a2[] = { 2,31,25,45,23,43,3,35,64 };
	for (auto e : a1)
	{
		test1.set(e);
	}
	for (auto e : a2)
	{
		test2.set(e);
	}

	for (int i = 0; i < 100; i++)
	{
		if (test1.test(i) == true && test2.test(i)==true)
		{
			cout << i << "->" << "交集" << endl;
		}
	}
}