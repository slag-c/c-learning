数组中只出现一次的两个数字
牛客网：数组中只出现一次的两个数字
#include <algorithm>
#include <unordered_map>
class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     * 
     * @param nums int整型vector 
     * @return int整型vector
     */
    vector<int> FindNumsAppearOnce(vector<int>& nums) {
        // write code here
        vector<int> v1;
        unordered_map<int, int> mapCount;
        for(int i = 0;i<nums.size();i++)
        {
            mapCount[nums[i]]++;
        }
        for(int i =0;i<nums.size();i++)
        {
            if(mapCount[nums[i]]==1)
            {
                v1.push_back(nums[i]);
            }
        }
        sort(v1.begin(), v1.end());
        return v1;
    }
};