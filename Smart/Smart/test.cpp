#include<iostream>
#include<functional>
using namespace std;
#include"SmartPtr.h"
#include<memory>

//int div()
//{
//	int a, b;
//	cin >> a >> b;
//	if (b == 0)
//		throw invalid_argument("除0错误");
//	return a / b;
//}
//void Func()
//{
//	SmartPtr<int> s1(new int);
//	SmartPtr<int> s2(new int);
//	/*int* p1 = new int;
//	int* p2 = new int;*/
//	cout << div() << endl;
//	/*delete p1;
//	delete p2;*/
//}
//int main()
//{
//	try
//	{
//		Func();
//	}
//	catch (exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	return 0;
//}

//int main()
//{
//
//
//	return 0;
//}
//
//int main()
//{
//	test::auto_ptr<int> a1(new int(10));
//
//	test::auto_ptr<int> a2(new int(20));
//
//	a1 = a2;
//
//
//	return 0;
//}


//int main()
//{
//	test::shared_ptr<int> p1(new int(10));
//	//cout << p1.use_count() << endl;
//	cout << p1.use_Count() << endl;
//	test::shared_ptr<int> p2(p1);
//	cout << p1.use_Count() << endl;
//	//cout << p1.use_count() << endl;
//	return 0;
//}


////循环引用例子
//struct ListNode {
//	int _val;
//	ListNode* _next;
//	ListNode* _prev;
//
//	ListNode(int val)
//		:_val(val),
//		_next(nullptr),
//		_prev(nullptr)
//	{}
//};
//
//int main()
//{
//	test::shared_ptr<ListNode> a1(new ListNode(2));
//	test::shared_ptr<ListNode> a2(new ListNode(3));
//
//	a1->_next = a2;
//	return 0;
//}


////循环引用例子
//struct ListNode {
//	int _val;
//	test::shared_ptr<ListNode> _next;
//	test::shared_ptr<ListNode> _prev;
//
//	ListNode(int val)
//		:_val(val),
//		_next(nullptr),
//		_prev(nullptr)
//	{}
//};
//
//int main()
//{
//	test::shared_ptr<ListNode> a1(new ListNode(2));
//	test::shared_ptr<ListNode> a2(new ListNode(3));
//
//	a1->_next = a2;
//	a2->_prev = a1;
//	return 0;
//}

//解决循环引用
//struct ListNode {
//	int _val;
//	test::weak_ptr<ListNode> _next;
//	test::weak_ptr<ListNode> _prev;
//
//	ListNode(int val)
//		:_val(val),
//		_next(nullptr),
//		_prev(nullptr)
//	{}
//
//	~ListNode()
//	{
//		cout << "~ListNode()" << endl;
//	}
//};
//
//int main()
//{
//	test::shared_ptr<ListNode> a1(new ListNode(2));
//	test::shared_ptr<ListNode> a2(new ListNode(3));
//
//	a1->_next = a2;
//	a2->_prev = a1;
//	return 0;
//}


int main()
{
	return 0;
}