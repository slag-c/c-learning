#pragma once
namespace test {

	template<class T>
	class SmartPtr {
	public:
		SmartPtr(T* ptr=nullptr)
			:_ptr(ptr)
		{}

		T operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}

		~SmartPtr()
		{
			if (_ptr)
			{
				cout << "delete" << endl;
				delete _ptr;
			}
		}
	private:
		T* _ptr;
	};

//模拟实现auto_ptr
	template<class T>
	class auto_ptr {
	public:
		auto_ptr(T* ptr)
			:_ptr(ptr)
		{}

		//拷贝构造  实现资源管理权的转移
		//a1(a2)
		auto_ptr(const auto_ptr<T>& p)
		{
			_ptr = p._ptr;
			p._ptr = nullptr;
		}

		//赋值拷贝
		auto_ptr<T>& operator=(auto_ptr<T>& p)
		{
			//自己没给自己赋值情况下才进行处理
			if (this != &p) {
				_ptr = p._ptr;
				p._ptr = nullptr;
				return *this;
			}
		}

		T operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}

		~auto_ptr()
		{
			if (_ptr)
			{
				cout << "delete" << endl;
				delete _ptr;
			}
		}
	private:
		T* _ptr;
	};


	//模拟实现unique_ptr
	template<class T>
	class unique_ptr {
	public:
		unique_ptr(T* ptr)
			:_ptr(ptr)
		{}
		//拷贝构造与赋值拷贝构造禁止使用！
		unique_ptr(const unique_ptr<T>& p) = delete;
		unique_ptr<T>& operator=(unique_ptr<T>& p) = delete;

		T operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}

		~unique_ptr()
		{
			if (_ptr)
			{
				cout << "delete" << endl;
				delete _ptr;
			}
		}
	private:
		T* _ptr;
	};

	////模拟实现shared_ptr
	////利用引用计数
	//template<class T>
	//class shared_ptr {
	//public:
	//	shared_ptr(T* ptr)
	//		:_ptr(ptr),
	//		_Count(new int(1))
	//	{}
	//	//s1(s2)
	//	shared_ptr(const shared_ptr<T>& p)
	//	{
	//		_ptr = p._ptr;
	//		_Count = p._Count;
	//		(*_Count)++;
	//	}

	//	//s1 = s2
	//	shared_ptr<T>& operator=(const shared_ptr<T>& p)
	//	{
	//		//只有不是指向同一块资源的才可以进行赋值操作，对计数进行改变
	//		if (_ptr != p._ptr)
	//		{
	//			//如果被赋值对象仅有它自己指向一块空间
	//			//那么此时就必须先对资源进行清理
	//			//在进行赋值
	//			destory();
	//			_ptr = p._ptr;
	//			_Count = p._Count;
	//			(*_Count)++;
	//		}
	//	}

	//	//计数，引用计数的个数
	//	int use_Count()
	//	{
	//		return *_Count;
	//	}

	//	T operator*()
	//	{
	//		return *_ptr;
	//	}

	//	T* operator->()
	//	{
	//		return _ptr;
	//	}

	//	~shared_ptr()
	//	{
	//		destory();
	//	}

	//	T* getPtr()
	//	{
	//		return _ptr;
	//	}
	//private:

	//	void destory()
	//	{
	//		if (--(*_Count)==0)
	//		{
	//			cout << "delete" << endl;
	//			delete _ptr;
	//			delete _Count;
	//		}
	//	}
	//	T* _ptr;
	//	int* _Count;
	//};


	//模拟实现weak_ptr
	template<class T>
	class weak_ptr {
	public:
		weak_ptr(T* ptr)
			:_ptr(ptr)
		{}
	
		weak_ptr(const shared_ptr<T>& p)
		{
			_ptr =p.getPtr();
		}

		//s1 = s2
		weak_ptr<T>& operator=(const shared_ptr<T>& p)
		{
			_ptr = p.getPtr();
			return *this;
		}

		T operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}

	private:
		T* _ptr;
	};


	//引入删除器，实现shared_ptr
	template<class T>
	class shared_ptr {
	public:
		template<class D>
		shared_ptr(T* ptr,D del)
			:_ptr(ptr),
			_Count(new int(1)),
			_del(del)
		{}
		//s1(s2)
		shared_ptr(const shared_ptr<T>& p)
		{
			_ptr = p._ptr;
			_Count = p._Count;
			(*_Count)++;
		}

		//s1 = s2
		shared_ptr<T>& operator=(const shared_ptr<T>& p)
		{
			//只有不是指向同一块资源的才可以进行赋值操作，对计数进行改变
			if (_ptr != p._ptr)
			{
				//如果被赋值对象仅有它自己指向一块空间
				//那么此时就必须先对资源进行清理
				//在进行赋值
				destory();
				_ptr = p._ptr;
				_Count = p._Count;
				(*_Count)++;
			}
		}

		//计数，引用计数的个数
		int use_Count()
		{
			return *_Count;
		}

		T operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}

		~shared_ptr()
		{
			destory();
		}

		T* getPtr()
		{
			return _ptr;
		}
	private:

		void destory()
		{
			if (--(*_Count) == 0)
			{
				cout << "delete" << endl;
				_del(_ptr);
				delete _Count;
			}
		}
		T* _ptr;
		int* _Count;
		//采用包装器 构造的时候可以选择lambda表达式 函数指针 仿函数
		function<void(T*)> _del = [](T* ptr) {delete ptr; };
	};
}